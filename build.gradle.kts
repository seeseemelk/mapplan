plugins {
    id("java")
    id("io.freefair.lombok") version "8.4"
}

group = "be.seeseemelk"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.hibernate.orm:hibernate-core:6.3.0.Final")
    implementation("org.hibernate.orm:hibernate-community-dialects:6.3.0.Final")
    implementation("org.xerial:sqlite-jdbc:3.44.0.0")
    implementation("org.postgresql:postgresql:42.6.0")
    annotationProcessor("org.hibernate.orm:hibernate-jpamodelgen:6.3.0.Final")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}
