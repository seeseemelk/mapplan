package be.seeseemelk.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Road
{
	@Id
	private long id;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "roadId")
	private Set<Tag> tags;

	@ManyToMany
	@JoinTable(
			joinColumns =  { @JoinColumn(name = "roadId") },
			inverseJoinColumns = { @JoinColumn(name = "nodeId")}
	)
	private List<Node> nodes;
}
