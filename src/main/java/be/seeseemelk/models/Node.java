package be.seeseemelk.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Node
{
	@Id
	private long id;

	private double latitude;

	private double longitude;

//	@ManyToMany(mappedBy = "nodes")
//	private Set<Road> roads;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "nodeId")
	private Set<Tag> tags;
}
