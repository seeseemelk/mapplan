package be.seeseemelk.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tag
{
	@Id
	@GeneratedValue
	private long id;

	private String key;

	private String value;
}
