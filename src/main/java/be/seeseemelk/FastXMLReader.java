package be.seeseemelk;

import lombok.RequiredArgsConstructor;

import java.io.InputStream;
import java.io.Reader;

@RequiredArgsConstructor
public class FastXMLReader
{
	public static final int START_ELEMENT = 1;

	private enum State
	{
		START,
		TAG_START,
	}

	private final Reader input;
	private State state = State.START;

	public void mext()
	{
		switch (state)
		{
			case START -> nextTag();
		}
	}

	private void nextTag()
	{
		while (readChar() != '<');
		state = State.TAG_START;
	}
}
