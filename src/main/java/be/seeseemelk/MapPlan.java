package be.seeseemelk;

import be.seeseemelk.models.Node;
import be.seeseemelk.models.Road;
import be.seeseemelk.models.Tag;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.schema.Action;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@RequiredArgsConstructor
public class MapPlan
{
	private final SessionFactory sessions;

	public static void main(String[] args) throws XMLStreamException, IOException
	{
        try (var sessionFactory = new Configuration()
				.addAnnotatedClass(Node.class)
				.addAnnotatedClass(Road.class)
				.addAnnotatedClass(Tag.class)
//                .setProperty(AvailableSettings.URL, "jdbc:sqlite:map.sqlite3")
				.setProperty(AvailableSettings.URL, "jdbc:postgresql://localhost/mapplan?user=mapplan&password=mapplan")
//				.setProperty(AvailableSettings.JAKARTA_JDBC_URL, "jdbc:sqlite:map.sqlite3")
//				.setProperty(AvailableSettings.DIALECT, "org.hibernate.community.dialect.SQLiteDialect")
				.setProperty("hibernate.id.new_generator_mappings", "true")
				.setProperty(AvailableSettings.AUTOCOMMIT, "true")
//				.setProperty(AvailableSettings.ALLOW_UPDATE_OUTSIDE_TRANSACTION, "true")
				.setProperty(AvailableSettings.STATEMENT_BATCH_SIZE, "100000")
				.setProperty(AvailableSettings.ORDER_INSERTS, "true")
//				.setProperty(AvailableSettings.USE_QUERY_CACHE, "true")
//				.setProperty(AvailableSettings.PRAG, "true")
				.setProperty(AvailableSettings.HBM2DDL_AUTO, Action.ACTION_UPDATE)
				.setProperty(AvailableSettings.ORDER_UPDATES, "true")
//				.setProperty(AvailableSettings.SHOW_SQL, "true")
                .buildSessionFactory())
		{
//			sessionFactory.inSession(session -> {
//				session.createNativeQuery("PRAGMA locking_mode=off", Void.class).executeUpdate();
//			});

			try (var input = Files.newInputStream(Path.of("/home/seeseemelk/Downloads/belgium-latest.osm"), StandardOpenOption.READ))
			{
				try (var session = sessionFactory.openSession())
				{
					sessionFactory.getSchemaManager().dropMappedObjects(true);
					sessionFactory.getSchemaManager().exportMappedObjects(true);
					var parser = new OsmParser(session, new BufferedInputStream(input));
					parser.start();
					parser.read();
					parser.end();
				}
			}

			var mapPlan = new MapPlan(sessionFactory);
		}
	}
}
