package be.seeseemelk;

import be.seeseemelk.models.Node;
import be.seeseemelk.models.Road;
import be.seeseemelk.models.Tag;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OsmParser
{
	private static final int TOTAL_NODE_COUNT = 62_323_030 + 23_481_217;
	private static final XmlScope SKIP = null;
	private int nodeCount = 0;
	private int tagId = 0;
	private long startTime;

	public final Session session;
	public final XMLStreamReader xml;
	private XmlScope scope = new RootScope();

	public OsmParser(Session session, InputStream input) throws XMLStreamException
	{
		this.session = session;
		xml = XMLInputFactory.newInstance().createXMLStreamReader(input);
	}

	public void start()
	{
		startTime = System.currentTimeMillis();
		session.beginTransaction();
	}

	public void end()
	{
		session.getTransaction().commit();
	}

	public void read() throws XMLStreamException
	{
		while (xml.hasNext())
		{
			var element = xml.next();
			switch (element)
			{
				case XMLStreamReader.START_ELEMENT -> onStartElement();
				case XMLStreamConstants.END_ELEMENT -> onEndElement();
				case XMLStreamConstants.CHARACTERS -> {}
				default -> throw new UnsupportedOperationException("Unsupported element %d".formatted(element));
			}
		}
	}

	public boolean includePoint(double latitude, double longitude)
	{
//		return true;
//		return longitude >= 5.1265
//				&& longitude <= 5.3960
//				&& latitude >= 50.8841
//				&& latitude <= 50.9795;
		return 5.13907 <= longitude && longitude <= 5.14750
				&& 50.93394 <= latitude && latitude <= 50.93679;
	}

	private void onStartElement() throws XMLStreamException
	{
		var name = xml.getName().getLocalPart();
		var newScope = scope.onTag(name);
		if (newScope != SKIP)
		{
			newScope.onStart();
			newScope.parent = scope;
			scope = newScope;
		}
		else
			skipTag();
	}

	private void skipTag() throws XMLStreamException
	{
		int depth = 1;
		while (xml.hasNext() && depth > 0)
		{
			switch (xml.next())
			{
				case XMLStreamReader.START_ELEMENT -> depth++;
				case XMLStreamConstants.END_ELEMENT -> depth--;
			}
		}
	}

	private void onEndElement()
	{
		scope.onEnd();
		scope = scope.parent;
	}


	private Node getNode(long id)
	{
		return session.byId(Node.class).load(id);
	}

	private void incrementEntityCount()
	{
		nodeCount++;
		if ((nodeCount % 10_000) == 0)
		{
			System.out.println("Committing");
			session.getTransaction().commit();
			session.getTransaction().begin();

			var currentTime = System.currentTimeMillis();
			var runTime = (currentTime - startTime) / 1000.0;
			var recordsPerSecond = 10_000 / runTime;
			System.out.printf("Imported %dK records (%.1f Krecords/second, took %.2f seconds)%n", nodeCount / 1000, recordsPerSecond/1000, runTime);
			startTime = currentTime;
//			System.out.printf("Import %f%% complete%n", (nodeCount * 100.0) / (TOTAL_NODE_COUNT));
		}
	}

	private abstract class XmlScope
	{
		private XmlScope parent;

		XmlScope onTag(String tag)
		{
			throw new UnsupportedOperationException("Unsupported tag '%s'".formatted(tag));
		}

		void scanAttributes()
		{
			for (int i = 0; i < xml.getAttributeCount(); i++)
			{
				var name = xml.getAttributeName(i).getLocalPart();
				var value = xml.getAttributeValue(i);
				onAttribute(name, value);
			}
		}

		void onAttribute(String name, String value)
		{
			throw new UnsupportedOperationException("Unsupported attribute '%s' with value '%s'".formatted(name, value));
		}
		void onStart() {}
		void onEnd() {}
	}

	private abstract class AttributedXmlScope extends XmlScope
	{
		@Override
		void onStart()
		{
			scanAttributes();
		}
	}

	private class RootScope extends XmlScope
	{
		@Override
		XmlScope onTag(String tag)
		{
			return switch (tag)
			{
				case "osm" -> new OsmScope();
				default -> super.onTag(tag);
			};
		}
	}

	private class OsmScope extends XmlScope
	{
		@Override
		XmlScope onTag(String tag)
		{
			return switch (tag)
			{
				case "node" -> new NodeScope();
//				case "way" -> new WayScope();
				case "bounds", "way", "relation" -> SKIP;
				default -> super.onTag(tag);
			};
		}
	}

	private class NodeScope extends AttributedXmlScope
	{
		private long id;
		private double latitude;
		private double longitude;
		private Set<Tag> tags = new HashSet<>();

		@Override
		void onStart()
		{
			super.onStart();
		}

		@Override
		XmlScope onTag(String tag)
		{
			return switch (tag)
			{
				case "tag" -> new TagScope(this.tags);
				default -> super.onTag(tag);
			};
		}

		@Override
		void onAttribute(String name, String value)
		{
			switch (name)
			{
				case "id" -> id = Long.parseLong(value);
				case "lat" -> latitude = Double.parseDouble(value);
				case "lon" -> longitude = Double.parseDouble(value);
				case "version", "timestamp" -> {}
				default -> super.onAttribute(name, value);
			}
		}

		@Override
		void onEnd()
		{
			if (includePoint(latitude, longitude))
			{
				session.persist(Node.builder()
						.id(id)
						.latitude(latitude)
						.longitude(longitude)
						.tags(tags)
						.build());

				incrementEntityCount();
			}
		}
	}

	@RequiredArgsConstructor
	private class TagScope extends AttributedXmlScope
	{
		private final Set<Tag> tags;
		private String key;
		private String value;

		@Override
		void onAttribute(String name, String value)
		{
			switch (name)
			{
				case "k" -> key = value;
				case "v" -> this.value = value;
				default -> super.onAttribute(name, value);
			}
		}

		@Override
		void onEnd()
		{
			var tag = Tag.builder()
					.key(key)
					.value(value)
					.build();
			tags.add(tag);
		}
	}

	private class WayScope extends AttributedXmlScope
	{
		private long id;
		private List<Node> nodes = new ArrayList<>();
		private Set<Tag> tags = new HashSet<>();

		@Override
		XmlScope onTag(String tag)
		{
			return switch (tag)
			{
				case "nd" -> new WayNdScope(nodes);
				case "tag" -> new TagScope(tags);
				default -> super.onTag(tag);
			};
		}

		@Override
		void onAttribute(String name, String value)
		{
			switch (name)
			{
				case "id" -> id = Long.parseLong(value);
				case "user", "uid", "visible", "version", "changeset", "timestamp" -> {}
				default -> super.onAttribute(name, value);
			}
		}

		@Override
		void onEnd()
		{
			if (!tags.isEmpty())
			{
				var road = Road.builder()
						.id(id)
						.tags(tags)
						.nodes(nodes)
						.build();
				session.persist(road);
				incrementEntityCount();
			}
		}
	}

	@RequiredArgsConstructor
	private class WayNdScope extends AttributedXmlScope
	{
		private final List<Node> nodes;

		@Override
		void onAttribute(String name, String value)
		{
			switch (name)
			{
				case "ref" -> {
					var node = getNode(Long.parseLong(value));
					if (node != null)
						nodes.add(node);
				}
				default -> super.onAttribute(name, value);
			};
		}
	}
}
